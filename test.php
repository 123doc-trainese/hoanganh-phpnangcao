<?php
class test{
    private $number1 = 999;

    //tu dong chay khi tao doi tuong moi voi cac tham so va rang buoc
    public function __construct($id, $text)
    {
        $this->id = $id;
        $this->text = $text;
    }

    //tu dong chay method __get khi truy cap pham vi khong duoc phep
    function __get($name)
    {
        return $this->$name;
    }
    
}
$tweet = new test(123, 'Hello world');

// echo $tweet->id;
// echo $tweet->text;

echo $tweet->number1;




