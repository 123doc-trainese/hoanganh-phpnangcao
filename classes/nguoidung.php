<?php

class nguoidung{
    
    protected static $name;

    public static function set_name($name){
        self::$name=$name;
    }
    
    public static function get_name(){
        return self::$name;
    }
    
    public static function all($name){
        self::set_name($name);
        return self::get_name();
    }
}