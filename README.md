# Lý thuyết
- Tìm hiểu về Magic method trong PHP OOP
- Tìm hiểu về autoload trong PHP
- Tìm hiểu thuật toán được sử dụng bên trong một số hàm builtin php : sắp xếp, so sánh, tìm kiếm, thay thế, ...
- Tìm hiểu các điểm mới trong phiên bản PHP mới nhất so với các bản còn trong thời gian support
- Đọc https://github.com/jupeter/clean-code-php#single-responsibility-principle-srp

# Kiến thức thu được
### Magic method
- là các phương thức đặc biệt để tùy biến các sự kiện trong php. nó cung cấp thêm cách để giải quyết một vấn đề xử lý các đối tượng trong lập trình hướng đối tượng.
- vi du: 
  - hàm __construct(): tự động được gọi khi khởi tạo 1 đối tượng
  - hàm __destruct(): được gọi khi đối tượng bị hủy
  - __set(): gọi khi ta truyền dữ liệu vào thuộc tính không tồn tại hoặc thuộc tính private trong đối tượng
  - __get(): gọi khi ta truy cập vào thuộc tính không tồn tại hoặc thuộc tính private trong đối tượng. Tương tự set, get xử lý khi truy cập đối tượng
  - __isset(): gọi khi chúng ta thực hiện kiểm tra một thuộc tính không được phép truy cập của một đối tượng, hay kiểm tra một thuộc tính không tồn tại trong đối tượng đó
  - __unset(): gọi khi hàm unset() được sử dụng trong một thuộc tính không được phép truy cập.
  - __call(): gọi một phương thức không được phép truy cập trong phạm vi của một đối tượng. __get() và __call() cũng gần giống nhau nhung __get() gọi khi không có thuộc tính còn __call() khi không có phương thức 
  - __callstatic(): khi gọi một phương thức không được phép truy cập trong phạm vi của một phương thức tĩnh.
  - __toString(): được gọi khi chúng ta in echo đối tượng (bắt buộc trả về một dãy string)
  - __invoke(): được gọi khi ta cố gắng gọi một đối tượng như một hàm
  - __Sleep(): gọi khi serialize() một đối tượng. Thông thường khi chúng ta serialize() một đối tượng thì nó sẽ trả về tất cả các thuộc tính trong đối tượng đó. Nhưng nếu sử dụng __sleep() thì chúng ta có thể quy định được các thuộc tính có thể trả về
  - __wakeup: Được gọi khi unserialize() đối tượng
  - __set_state(): Được sử dụng khi chúng ta var_export một object
  - __clone(): Được sử dụng khi chúng ta clone một object(sao chép 1 đối tượng thành 1 đối tượng hoàn toàn mới không liên quan đến đối tượng cũ) .
  -  __debugInfo(): Được gọi khi chúng ta sử dụng hàm vardump().


### autoload
- trong dự án lớn có nhiều không thể sử dụng require hoặc include từng file vì vậy việc sử dụng autoload là cần thiết.
- sử dụng hàm spl_autoload_register() để gọi vào nhiều file 1 lúc. đặt tên file theo tên class.
tạo một hàm autoload ở đây là hàm loader() để require file theo url ‘<tên folder>/<tên class>.<phần mở rộng của file>’

### Built-in Functions
- là các Functions có sẵn khi cài đặt PHP.
- 2 loại Builtin Functions là:
   - String Functions: Các hàm thao tác với Chuỗi trong PHP
   - Numeric Functions: Các hàm thao tác với Số trong PHP
- Hàm xử lý chuỗi thường dùng trong php:
   - strlen($string): Lấy độ dài của một chuỗi
   - explode ( $delimiter , $string): chuyển 1 chuỗi $string thành 1 mảng phần tử với ký tự tách mảng $delimiter
   - str_word_count($string): Đếm số lượng từ trong một chuỗi
   - strrev($string): Đảo ngược chuỗi
   - strtoupper($string): Chuyển toàn bộ chuỗi thành chữ in hoa
   - strtolower($string): Chuyển toàn bộ chuỗi thành chữ in thường
   - crc32 ( $str ): Hàm chuyển chuỗi $str thành dãy số nguyên
   - ord ( $string ): Hàm trả mã ASCII của ký tự đầu trong chuỗi $string
   - ucwords($string): Chuyển đổi chữ cái đầu tiên của mỗi từ từ chữ in thường sang chữ in hoa
  
#### Vi du:
```
<?php
echo ucwords(“welcome to the php”);
?>
```
#### Kết quả
Welcome To The Php
   - str_replace($find, $replace, $string): Thay thế đoạn văn bản trong một chuỗi
 
 #### Vi du:
```
<?php
echo str_replace(“Home”, “Office”, “Welcome to the Home);
?>
```

#### Kết quả
Welcome to the Office
  
   - str_repeat($string, $repeat): Lặp lại chuỗi
#### Vi du:
```
<?php
echo str_repeat(“.”,13);
?>
```
#### Kết quả
 ............

   - strcmp($string): So sánh các chuỗi
   So sánh 2 chuỗi trả về kết quả lớn hơn, bé hơn hoặc bằng 0.

++ Nếu chuỗi 1 nhỏ hơn chuỗi 2 thì kết quả trả về sẽ lớn hơn  0.
++ Nếu chuỗi 1 nhỏ hơn chuỗi 2 thì kết quả trả về sẽ bé hơn 0.
++ Nếu kết quả trả về bằng 0 thì đồng nghĩa với việc 2 chuỗi này bằng nhau
   - substr(string,start,length): Hiển thị một phần của chuỗi hoặc trích xuất một chuỗi ở một vị trí cụ thể hay hàm cắt chuỗi trong PHP hay tách chuỗi
#### Vi du:
```
<?php
echo substr("abcdef", 0, 3) . "<br />";  

?>
```
#### Kết quả
abc

   - trim($str, $char): loại bỏ kí tự có trong chuỗi
#### Vi du:
```
<?php
$trimmed = trim('Hello World', "Hdle");
var_dump($trimmed);
echo "<br />";
?>
```
#### Kết quả
string(5) "o Wor" 

   - strpos( $str, $char, $position): tìm kiếm vị trí đầu tiên của kí tự hoặc chuỗi con xuất hiện trong chuỗi nguồn
#### Vi du:
```
<?php
$str = 'abcdefghdiklm';<br />
$pos = strpos($str, 'd', 5);<br />
echo $pos;
?>
```
#### Kết quả
8
   - 

- cấu trúc của một hàm builtin php [xem thêm](https://github.com/php/php-src/blob/4b483cdc9a3fb28bbd16eec49aec9308b51be9fc/ext/standard/string.c#L1955)

### Phiên bản PHP 8.0 có gì mới
 - Union Types.

Kiểu dữ liệu cho tham số và dữ liệu trả về có thể là nhiều kiểu khác nhau

```
public function foo(Foo|Bar $input): int|float;
```

 - JIT – Just in time
JIT ra đời giúp thực hiện các tác vụ liên quan tới nhiều tính toán, xử lý hơn
-  Một số thay đổi khác trong PHP8:
   - Hàm str_contains để kiểm tra 1 chuỗi có nằm trong 1 chuỗi khác
   - Attributes – giống với annotations trong Java
   - Stringable  Interface

Thực hiện bởi [Nguyen Hoang Anh](https://github.com/anhbk177)

## Liên kết
 
[hoanganh-php-nang-cao](https://gitlab.com/123doc-trainese/hoanganh-htmlcss)
